import Vue from 'vue'
import Router from 'vue-router'
import Introduction from '@/components/Introduction'
import Quiz from '@/components/Quiz'
import AnimatedVideo from '@/components/AnimatedVideo'
import ComicStrip from '@/components/ComicStrip'
import Instruction from '@/components/Instruction'
import Activity from '@/components/Activity'
import Speech from '@/components/Speech'
import Reflection from '@/components/Reflection'

Vue.use(Router)

export default new Router({
  base: '/verbal-irony/',
  mode: 'history',
  routes: [
    {
      path: '/',
      name: 'Introduction',
      component: Introduction,
      meta: {
        title: 'Introduction'
      }
    },
    {
      path: '/quiz',
      name: 'Quiz',
      component: Quiz,
      meta: {
        title: 'Quiz'
      }
    },
    {
      path: '/animated-video',
      name: 'AnimatedVideo',
      component: AnimatedVideo,
      meta: {
        title: 'Animated Video'
      }
    },
    {
      path: '/comic-strip',
      name: 'ComicStrip',
      component: ComicStrip,
      meta: {
        title: 'Comic Strip'
      }
    },
    {
      path: '/instruction',
      name: 'Instruction',
      component: Instruction,
      meta: {
        title: 'Instruction'
      }
    },
    {
      path: '/speech',
      name: 'Speech',
      component: Speech,
      meta: {
        title: 'Speech'
      }
    },
    {
      path: '/activity',
      name: 'Activity',
      component: Activity,
      meta: {
        title: 'Activity'
      }
    },
    {
      path: '/reflection',
      name: 'Reflection',
      component: Reflection,
      meta: {
        title: 'Reflection'
      }
    }
  ]
})
