import Vue from 'vue'
import Vuex from 'vuex'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    quizzes: [
      {
        id: 1,
        choices: [
          {
            id: 1,
            option: 'A',
            media: ''
          },
          {
            id: 2,
            option: 'B',
            correct: true,
            media: ''
          }
        ]
      },
      {
        id: 2,
        choices: [
          {
            id: 1,
            option: 'A',
            correct: true,
            media: ''
          },
          {
            id: 2,
            option: 'B',
            media: ''
          }
        ]
      }
    ],
    comicStrips: [
      {
        id: 1,
        name: ''
      },
      {
        id: 2,
        name: ''
      },
      {
        id: 3,
        name: ''
      },
      {
        id: 4,
        name: ''
      },
      {
        id: 5,
        name: ''
      },
      {
        id: 6,
        name: ''
      }
    ],
    speeches: [
      {
        id: 1
      },
      {
        id: 2
      }
    ],
    ironies: [
      {
        id: 1,
        text: 'Friends, Romans, countrymen, lend me your ears;',
        correct: false,
        feedback: 'In the 1st line, Mark Antony is simply calling out to the crowd to pay attention and listen to what he has to say.'
      },
      {
        id: 2,
        text: 'I come to bury Caesar, not to praise him.',
        correct: true,
        feedback: '<p>Well Done!</p>' +
        'Here, Mark Antony says that his purpose for speaking at Caesar\'s funeral is merely to lay the dead Caesar to rest, rather than to pay tribute to him. This is ironic because Mark Antony does subtly remind the crowd of Caesar\'s good qualities in the rest of the speech.'
      },
      {
        id: 3,
        text: 'The evil that men do lives after them; The good is often interred with their bones; So let it be with Caesar.',
        correct: true,
        feedback: '<p>Good Job!</p>' +
        'In the first 2 lines, Mark Antony is saying that the evil that emn do is usually remembered after their deaths, while the good is often forgotten, and it should be the same with Caesar. Note that the first two lines are not ironic in themselves as it a fact that many notorious people in history are remembered for their evil deeds, which tend to overshadow any good they have done in their lives.</p>' +
        'However, Mark Antony\'s last line "So let it be with Caesar" makes the whole statement ironic as he does not intend to highlight Caesar\'s flaws, but remind the crowd of his good qualities in the rest of his speech.</p>'
      },
      {
        id: 4,
        text: 'The noble Brutus Hath told you Caesar was ambitious;',
        correct: true,
        feedback: '<p>Excellent</p>' +
        'Here, Mark Antony calls Brutus "noble", which he does not mean, as he feels that Brutus\' murder of Caesar was unjust. Thus, verbal irony is employed as he means the opposite of what he says.</p>'
      },
      {
        id: 5,
        text: 'If it were so, it was a grievous fault, And grievously hath Caesar answer’d it.',
        correct: false,
        feedback: 'Mark Antony is stating a fact here: If Caesar had been an ambitious tyrant, it would have been serious fault, and he would already have paid for it gravely with his life.'
      },
      {
        id: 6,
        text: 'Here, under leave of Brutus and the rest',
        correct: false,
        feedback: 'Here, Mark Antony is saying that it was only with the permission of Brutus and the rest that he has come to speak at Caesar\'s funeral. This is a fact as Brutus allowed Mark Antony to have an audience with the crowd.'
      },
      {
        id: 7,
        text: 'For Brutus is an honorable man; So are they all, all honorable men',
        correct: true,
        feedback: '<p>Well Done!</p>' +
        'Here, Mark Antony calls Brutus and the conspirators "honourable men", which he does not mean, as he feels that their murder of Caesar was unjust. Thus, verbal irony is employed as he means the opposite of what he says.'
      },
      {
        id: 8,
        text: 'Come I to speak in Caesar’s funeral. He was my friend, faithful and just to me:',
        correct: false,
        feedback: 'Here, Mark Antony calls Caesar a friend who was loyal to him, which is true in the context of this play.'
      },
      {
        id: 9,
        text: 'But Brutus says he was ambitious; And Brutus is an honorable man.',
        correct: true,
        feedback: '<p>Excellent!</p>' +
        'Here, Mark Antony calls Brutus an "honourable man", which he does not mean, as he feels that his murder of Caesar was unjust. This, verbal irony is employed as he means the opposite of what he says. Do note that Antony repeats this line from before, heightening the verbal irony.'
      },
      {
        id: 10,
        text: 'He hath brought many captives home to Rome Whose ransoms did the general coffers fill: Did this in Caesar seem ambitious? When that the poor have cried, Caesar hath wept: Ambition should be made of sterner stuff:',
        correct: false,
        feedback: 'In these lines, Mark Antony lists Caesar\'s good qualities. He states that Caesar brought many captives home to Rome, and their randoms brought wealth to the city. When the poor suffered and wept, Caesar empathised with them too, feeling their pain as if it was his own. Anton states that an amibitious man would have been firmer and stronger, not compassionate.'
      },
      {
        id: 11,
        text: 'Yet Brutus says he was ambitious; And Brutus is an honorable man.',
        correct: true,
        feedback: '<p>Good Job!</p>' +
        'Here, Mark Antony calls Brutus an "honourable man", which he does not mean, as he feels that his murder of Caesar was unjust. Thus, verbal irony is employed as he means the opposite of what he says. Do note that this is the second time Antony repeats this line, heightening the verbal irony.'
      },
      {
        id: 12,
        text: 'You all did see that on Lupercal I thrice presented him a kingly crown, Which he did thrice refuse: was this ambition?',
        correct: false,
        feedback: 'In this line, Mark Antony questions the crowd if they still feel that Caesar is ambitious when he refused the crown three times after Antony offered it during a feast.'
      },
      {
        id: 13,
        text: 'Yet Brutus says he was ambitious; And, sure, he is an honorable man. I speak not to disprove what Brutus spoke,  But here I am to speak what I do know.',
        correct: true,
        feedback: '<p>Good Job!</p>' +
        '<p>Here, Mark Antony calls Brutus an "honourable man", which he does not mean, as he feels that his murder of Caesar was unjust. Do note that this is the third time Antony repeats this line, heightening the verbal irony.</p>' +
        '<p>Similarly, he claims that he is not here to "disprove what Brutus spoke" about Caesar, which is untrue because his intention is to undermine Brutus and turn the crowd against him.</p>' +
        '<p>Thus, verbal irony is employed as he means the opposite of what he says.</p>'
      }
    ]
  },
  mutations: {},
  getters: {
    animatedVideos (state) {
      return state.quizzes.map(quiz => quiz.id)
    },
    speeches (state) {
      return state.speeches.map(speech => speech.id)
    }
  }
})
