# Changelog

## v2.0.0
### Breaking changes
- Upgrade to Webpack v4

## 1.0.0
### Initial release
- Updated comic assets with colour
- Massive styling added to whole site
- Added animated videos

## 0.3.0
- Added audio assets
- Added styling to Activity page

## 0.2.1
- Added a button to skip to Speech in comic strip page
- Updated comic strip
- Fixed videos causing page to "scroll"
- Fixed video having a non-black background colour

## 0.2.0
- Added a button on Comic Strip modal
- Added Mark Anthony speech
- Fixed and improved styling for animated video component
- Fixed responsiveness in Comic Strip

## 0.1.0
Internal initial release
