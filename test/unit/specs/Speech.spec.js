import { shallow, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Speech from '@/components/Speech'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Speech', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallow(Speech, {
      mocks: {
        $store: {
          getters: {}
        }
      },
      localVue
    })
  })

  test('should navigate correctly if button is clicked', () => {
    const router = new VueRouter()

    wrapper = shallow(Speech, {
      mocks: {
        $store: {
          getters: {
            speeches: {}
          }
        }
      },
      router,
      localVue
    })

    wrapper.setData({
      videoEnded: true
    })

    wrapper.find('button').trigger('click')

    expect(wrapper.vm.$route.path).toBe('/activity')
  })
})
