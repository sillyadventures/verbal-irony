import { shallow } from '@vue/test-utils'
import Introduction from '@/components/Introduction'

describe('Introduction', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallow(Introduction)
  })

  test('should render correct title', () => {
    expect(wrapper.find('h1').text()).toBe('What is Verbal Irony?')
  })
})
