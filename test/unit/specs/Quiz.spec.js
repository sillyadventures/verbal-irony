import { shallow, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import Quiz from '@/components/Quiz'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('Quiz', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallow(Quiz, {
      mocks: {
        $store: {
          state: {
            quizzes: [
              {
                id: 1
              }
            ]
          }
        }
      },
      localVue
    })
  })

  test('should render correct title', () => {
    expect(wrapper.find('h1').text()).toBe('Verbal Irony')
  })

  test('defaults quiz index to 0', () => {
    expect(wrapper.vm.quizIndex).toBe(0)
  })
})
