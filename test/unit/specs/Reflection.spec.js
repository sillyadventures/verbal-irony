import { shallow } from '@vue/test-utils'
import Reflection from '@/components/Reflection'

describe('Reflection', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallow(Reflection)
  })

  test('should render a video on load', () => {
    expect(wrapper.find('video')).toBeTruthy()
  })
})
