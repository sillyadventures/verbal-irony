import { shallow, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import QuizImage from '@/components/QuizImage'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('QuizImage', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallow(QuizImage, {
      propsData: {
        quiz: {
          id: 1
        },
        choice: {
          option: 'A'
        }
      },
      computed: {
        image: () => 'quiz-1-a.jpg'
      }
    })
  })

  test('button should navigate to correct path', () => {
    wrapper.setData({
      choice: {
        id: 1,
        correct: true
      }
    })

    wrapper.find('img').trigger('click')

    expect(wrapper.vm.$emit('incrementIndex')).toBeTruthy()
  })
})
