import { shallow, createLocalVue } from '@vue/test-utils'
import VueRouter from 'vue-router'
import Instruction from '@/components/Instruction'

const localVue = createLocalVue()
localVue.use(VueRouter)

describe('Instruction', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallow(Instruction)
  })

  test('shows button when video has finished playing', () => {
    wrapper.setData({
      showNextButton: true
    })

    expect(wrapper.find('button').text()).toBe('Mark Antony\'s Speech')
  })

  test('navigate correctly when button is clicked', () => {
    const router = new VueRouter()

    wrapper = shallow(Instruction, {
      router, localVue
    })

    wrapper.setData({
      showNextButton: true
    })

    wrapper.find('button').trigger('click')

    expect(wrapper.vm.$route.path).toBe('/speech')
  })
})
