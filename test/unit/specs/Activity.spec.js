import { shallow, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import Activity from '@/components/Activity'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('Activity', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallow(Activity, {
      mocks: {
        $store: {
          state: {
            ironies: [
              {
                id: 1,
                text: '',
                correct: true,
                feedback: ''
              }
            ]
          }
        }
      },
      localVue
    })
  })

  test('should increment correct count', () => {
    window.HTMLMediaElement.prototype.load = () => {}
    window.HTMLMediaElement.prototype.play = () => {}

    expect(wrapper.vm.correctCount).toBe(0)

    wrapper.findAll('.is-choice').at(0).trigger('click')

    expect(wrapper.vm.correctCount).toBe(1)
  })

  test('should navigate correctly when button is clicked', () => {
    const router = new VueRouter()

    wrapper = shallow(Activity, {
      computed: {
        correctAnswerRemaining: () => 0
      },
      router,
      localVue
    })

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.$route.path).toBe('/reflection')
  })
})
