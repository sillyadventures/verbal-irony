import { shallow, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import ComicStrip from '@/components/ComicStrip'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('ComicStrip', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallow(ComicStrip, {
      mocks: {
        $store: {
          state: {}
        }
      },
      localVue
    })
  })

  test('should navigate correctly when button is clicked', () => {
    const router = new VueRouter()

    wrapper = shallow(ComicStrip, {
      router, localVue
    })

    wrapper.find('.is-next').trigger('click')

    expect(wrapper.vm.$route.path).toBe('/instruction')
  })
})
