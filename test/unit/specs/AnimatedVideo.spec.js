import { shallow, createLocalVue } from '@vue/test-utils'
import Vuex from 'vuex'
import VueRouter from 'vue-router'
import AnimatedVideo from '@/components/AnimatedVideo'

const localVue = createLocalVue()
localVue.use(Vuex)
localVue.use(VueRouter)

describe('AnimatedVideo', () => {
  let wrapper

  beforeEach(() => {
    wrapper = shallow(AnimatedVideo)
  })

  test('defaults does not show overlay', () => {
    expect(wrapper.vm.videoEnded).toBe(false)
  })

  test('shows overlay with button when video has ended', () => {
    wrapper.setData({
      videoEnded: true
    })

    expect(wrapper.find('button').text()).toBe('Read Comic')
  })

  test('should navigate to correct path', () => {
    const router = new VueRouter()

    const wrapper = shallow(AnimatedVideo, {
      mocks: {
        $store: {
          getters: {
            animatedVideos: () => []
          }
        }
      },
      router,
      localVue
    })

    wrapper.setData({videoEnded: true})

    wrapper.find('button').trigger('click')

    expect(wrapper.vm.$route.path).toBe('/comic-strip')
  })
})
